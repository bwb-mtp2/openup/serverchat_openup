const mongoose = require("mongoose");
const express = require("express");
const app = express();
var cors = require("cors");
require("dotenv").config();
const server = require("http").createServer(app);
const io = require("socket.io")(server, { cors: { origin: "*" } });
const url = process.env.URL;
const PORT = process.env.PORT;
const ChatUser = require("./models/ChatUser");
const Order = require("./models/Order");

mongoose.connect(url, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

app.get('/', function(req, res) {
    res.send('CHAT EXPRESS WORK');
});

io.on("connection", async (socket) => {

  // On récupère les ID de la commande et de l'utilisateur
  const idOrder = socket.handshake.query.idOrder;
  const idUser = socket.handshake.query.idUser;
  // On récupère le tableau de discussion par l'id de commande
  let chat = await ChatUser.findOne({idOrder: idOrder});

  if(chat === null){
    chat = new ChatUser({
      idOrder: idOrder
    })
    await chat.save();
  }

  // On récupere l'objet order par l'id de commande
  const order = await Order.findById(idOrder);

  // On détecte si nous sommes client ou livreur
  const whoAmI = order.user.idUser === idUser ? "messageCustomer" : "messageDeliverer";

  // On récupère l'id de notre interlocuteur
  const idOtherUser = order.user.idUser === idUser ? order.deliverer.idDeliverer : order.user.idUser;

  socket.on(idOrder + "-" + idUser + "-typing", (value) => {
    //isTyping  received the event to send to the user
    console.log("on typing", value);
    io.emit(idOrder + "-" + idUser + "-typing", value);
  });

  socket.on("disconnect", () => {
    console.log(idUser + " disconnected");
  });


  socket.on(idOrder + "-" + idUser + "-reading", (msgId) => {
    //renderTicks  received the event to send to the user
    console.log("on reading", msgId);
    io.emit(idOrder + "-" + idOtherUser + "-reading", msgId);
  });

  socket.on(idOrder + "-" + idUser + "-sending", async (data) => {
    
    chat[whoAmI].push({ _id: data._id, text: data.text });
    await chat.save();
    
    const last = chat[whoAmI][chat[whoAmI].length - 1];
    io.emit(idOrder + "-" + idOtherUser, {
      _id: last._id,
      text: data.text,
      idUser: idUser,
      date: last.date,
    });
  });
});

server.listen(PORT, () => {
  console.log(`Chat server has started at : http://localhost:${PORT}`);
});
